
const errormessage = '\
    <div class="ms-MessageBar ms-MessageBar--blocked">\
        <div class="ms-MessageBar-content">\
            <div class="ms-MessageBar-icon">\
                <i class="ms-Icon ms-Icon--Blocked"></i>\
            </div>\
            <div class="ms-MessageBar-text">\
                Please check all checkbox.\
            </div>\
        </div>\
    </div>';

$(function(){
    //add Graph api
    window.opener.__mailbox.getCallbackTokenAsync((result)=>console.log(result.value))
    console.log(window.opener.__mailbox)

    window.opener.__subject.getAsync((result) => $("#subject").append(("Subject: " + result.value)))
    Promise.all([
        createhtml(window.opener.__to,'TO'),
        createhtml(window.opener.__cc,'CC'),
        createhtml(window.opener.__bcc,'BCC')
    ]).then(function(data){
        $('#list').append(data[0]+data[1]+data[2]);
        var CheckBoxElements = document.querySelectorAll(".ms-CheckBox");
        for (var i = 0; i < CheckBoxElements.length; i++) {
            new fabric['CheckBox'](CheckBoxElements[i]);
        }
    })
});
function createhtml(address,target){
    return new Promise(function(resolve){
        address.getAsync(function(result){
            const html = insertAddress(result.value,target);
            resolve(html)
        })
    })
}
function Onsend() {
    if(refCheckBox()){
        window.opener.__parentEvent.completed({allowEvent : true});
        window.close();
    }else{
        $('#error').html(errormessage)
    }
}
function Notsend() {
    window.opener.__parentEvent.completed({allowEvent : false});
    window.close();
}
function insertAddress(addresslist,target){
		if(addresslist.length === 0){
			html = ('<div class="addresslist none"><h2 class="target">ーー'+target+'ーー</h2>')
		}else{
			html = ('<div class="addresslist"><h2 class="target">ーー'+target+'ーー</h2>')
		}
    html += '<div class="list">'
    for(var i=0; i < addresslist.length; i++){
        html += '<span class="ms-CheckBox">'
        html += '<input tabindex="-1" type="checkbox" class="ms-CheckBox-input" />'
        html += ('<label role="checkbox" class="ms-CheckBox-field" tabindex="0" aria-checked="false" id="'+target+i+'">')
        html += ('<span class="ms-Label">'+addresslist[i].emailAddress+'</span>')
        html += '</label></span>'
        html += '&emsp;'
    }
		html += '</div></div>'
    return html
}
function refCheckBox(){
    var arr_checkBoxes = document.getElementsByClassName("ms-CheckBox-input");
    var count = 0;
    flg = true;
	for (var i = 0; i < arr_checkBoxes.length; i++) {
		if (!(arr_checkBoxes[i].checked)) {
			flg = false;
		}
    }
    return flg
}
