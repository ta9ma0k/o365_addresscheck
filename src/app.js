
var mailboxitem;
var dialog;

Office.initialize = function(reason){
    mailboxitem = Office.context.mailbox.item;
    window.__mailbox = Office.context.mailbox;
}

function Checkbtn(arg){
    dialog.close();
    if(arg.message == "OK") {
        __parentEvent.completed({allowEvent: true});
    }else{
        __parentEvent.completed({allowEvent: false});
    }
}

function test(event){
    window.__parentEvent = event;
    window.__subject = mailboxitem.subject;
    window.__to = mailboxitem.to;
    window.__cc = mailboxitem.cc;
    window.__bcc = mailboxitem.bcc;

    var top = screen.height / 2;
    var left = screen.width / 2;
    var WIDTH = 640;
    var HEIGHT = 480;
    var x = left - (WIDTH / 2);
    var y = top - (HEIGHT / 2);

    var newWindow = window.open('https://localhost:443/outlookAddin/src/popup.html', null, 'top=' + y + ',left=' + x + ',width=640,height=480');
    var interval = 0;
    interval = setInterval(function(){
        if(newWindow.closed){
            clearInterval(interval);
            __parentEvent.completed({allowEvent : false});
        }
    },1000);
}

